
#include<stdio.h>
void read_matrix(int mat[2][2],int r,int c);
void mul_matrix(int mat1[2][2],int mat2[2][2],int r,int c); 
void display_matrix(int mat[2][2],int r, int c);
int main()
{
int row,col;
int mat1[2][2],mat2[2][2];
printf("\n Enter the number of rows and columns of the matrix: "); 
scanf("%d%d",&row, &col);
printf("\n Enter the first matrix: ");
read_matrix(mat1,row,col);
printf("\n Enter the second matrix: ");
read_matrix(mat2,row,col);
if(col==row)
{
mul_matrix(mat1,mat2,row,col); 
}
else
{
printf("\n To multiply 2 matrices, number of columns in the first matrix must be equal to
number od rows in the second matrix");
}
return 0; 
}
void read_matrix(int mat[2][2], int r, int c)
{
int i,j;
for(i=0;i<r;i++)
{
printf("\n"); for(j=0;j<c;j++) 
{
printf("\t mat[%d][%d] =",i,j);
scanf("%d",&mat[i][j]); 
}
} 
}
void mul_matrix(int mat1[2][2],int mat2[2][2],int r,int c)
{
int i,j,k,prod[2][2];
for(i=0;i<r;i++)
{
for(j=0;j<c;j++)
{
prod[i][j] = 0;
for(k=0;k<c;k++)
prod[i][j] = mat1[i][k]*mat2[k][j];
}
}
display_matrix(prod,r,c); }
void display_matrix(int mat[2][2],int r, int c)
{
int i,j;
for(i=0;i<r;i++)
{
printf("\n"); 
for(j=0;j<c;j++)
{
printf("\t mat[%d][%d] = %d",i,j,mat[i][j]);
}
}
}