#include <stdio.h>
struct student
{
	int roll_no;
	char name[50];
	char section[5];
	char department[20];
	float fees;
	int result;
}
s1,s2;

struct student input(void)
{
	struct student s;
	printf("Enter student roll number:\n");
	scanf("%d",&s.roll_no);
	printf("Enter student name:\n");
	scanf("%s",s.name);
	printf("Enter student section:\n");
	scanf("%s",s.section);
	printf("Enter student department:\n");
	scanf("%s",s.department);
	printf("Enter student fees:\n");
	scanf("%f",&s.fees);
	printf("Enter student result:\n");
	scanf("%d",&s.result);
	return s;
}
void output(struct student a)
{
	printf("Student roll number: %d\n",a.roll_no);
	printf("Student name: %s\n",a.name);
	printf("Student section: %s\n",a.section);
	printf("Student department: %s\n",a.department);
	printf("Student fees: %f\n",a.fees);
	printf("Student result: %d\n",a.result);
}	
int main()
{
	printf("Enter first student details :\n\n");
	s1 = input();
	printf("\n\nEnter Second student details:\n\n");
	s2 = input();
	if(s1.result> s2.result)
	{
		output(s1);
	}
	else
	{
		output(s2);
	}
	return 0;	
}